package co.tiagoaguiar.chucknorrisio

import android.app.Application
import co.tiagoaguiar.chucknorrisio.common.module.NetworkModule
import co.tiagoaguiar.chucknorrisio.jokes.categories.component.CategoryRepositoryComponent
import co.tiagoaguiar.chucknorrisio.jokes.categories.component.DaggerCategoryRepositoryComponent
import co.tiagoaguiar.chucknorrisio.jokes.random.component.DaggerJokeRepositoryComponent
import co.tiagoaguiar.chucknorrisio.jokes.random.component.JokeRepositoryComponent

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class ChuckNorrisApplication : Application() {

    private val mBaseUrl = "https://api.chucknorris.io/"

    var categoryRepositoryComponent: CategoryRepositoryComponent? = null
    var jokeRepositoryComponent: JokeRepositoryComponent? = null

    override fun onCreate() {
        super.onCreate()

        categoryRepositoryComponent = DaggerCategoryRepositoryComponent.builder()
            .networkModule(NetworkModule(mBaseUrl))
            .build()

        jokeRepositoryComponent = DaggerJokeRepositoryComponent.builder()
            .networkModule(NetworkModule(mBaseUrl))
            .build()
    }

}