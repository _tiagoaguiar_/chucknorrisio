package co.tiagoaguiar.chucknorrisio.common.model

import io.reactivex.disposables.CompositeDisposable

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface DataSource<S, T> {

    fun onSuccess(response: T)

    fun onError(message: String)

    fun transform(joke: S) : T

    fun onComplete()

    fun onSubscribe(it: CompositeDisposable)

}