package co.tiagoaguiar.chucknorrisio.common.util

import android.graphics.Color
import java.util.*

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class Colors private constructor(){

    companion object {
        fun randomColor(): Int {
            val random = Random()

            var r = random.nextInt(0xFF)
            var g = random.nextInt(0xFF)
            var b = random.nextInt(0xFF)

            val range = 0xA0

            if (r < range) r = range
            if (g < range) g = range
            if (b < range) b = range

            return Color.argb(0x80, r, g, b)
        }
    }

}