package co.tiagoaguiar.chucknorrisio.common.util

import javax.inject.Scope

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ViewScoped {
}