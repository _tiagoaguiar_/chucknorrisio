package co.tiagoaguiar.chucknorrisio.common.view

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import co.tiagoaguiar.chucknorrisio.R
import co.tiagoaguiar.chucknorrisio.common.view.ActivityUtils.Companion.setAnimationOnClosed

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
abstract class AbstractActivity : AppCompatActivity() {

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResID())

        val toolbar: Toolbar? = getToolbar()
        toolbar?.let {
            setSupportActionBar(it)
            supportActionBar?.title = getToolbarTitle()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                setAnimationOnClosed(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    abstract fun getLayoutResID(): Int

    abstract fun getToolbar() : Toolbar?

    open fun getToolbarTitle() : String {
        return getString(R.string.app_name)
    }

}