package co.tiagoaguiar.chucknorrisio.common.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ProgressBar
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.content_main.*

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
abstract class AbstractFragment<P> : Fragment(), BaseView<P> {

    protected var mPresenter: P? = null

    protected var mContext: Context? = null

    protected lateinit var mDisposable: CompositeDisposable

    private var mProgressBar: ProgressBar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayoutResID(), container, false)

        mProgressBar = activity?.progress_bar

        onCustomCreateView(view, container)

        return view
    }

    override fun setPresenter(presenter: P) {
        mPresenter = presenter
    }

    override fun setDisposable(disposable: CompositeDisposable) {
        mDisposable = disposable
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    override fun onDetach() {
        super.onDetach()
        mContext = null
    }

    override fun onPause() {
        super.onPause()
        mProgressBar?.let {
            it.visibility = GONE
        }
    }

    override fun showProgressBar() {
        mProgressBar?.let {
            it.visibility = VISIBLE
        }
    }

    override fun hideProgressBar() {
        mProgressBar?.let {
            it.visibility = GONE
        }
    }

    abstract fun getLayoutResID(): Int

    abstract fun onCustomCreateView(view: View, container: ViewGroup?)
}