package co.tiagoaguiar.chucknorrisio.common.view

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import co.tiagoaguiar.chucknorrisio.R
import org.jetbrains.annotations.NotNull

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class ActivityUtils private constructor() {

    companion object {

        fun setAnimationOnClosed(activity: Activity) {
            activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out)
        }

        fun setAnimationOnOpen(activity: Activity) {
            activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out)
        }

        fun addFragmentToActivity(@NotNull activity: FragmentActivity, @NotNull clazz: Class<out Fragment>): Fragment? {
            var fragment = activity.supportFragmentManager.findFragmentById(R.id.content_frame)

            if (fragment == null)
                fragment = Fragment.instantiate(activity, clazz.name)

            fragment?.let {
                if (!it.isAdded) {
                    val transaction = activity.supportFragmentManager.beginTransaction()
                    transaction.add(R.id.content_frame, it)
                    transaction.commit()
                }
            }

            return fragment
        }

    }

}