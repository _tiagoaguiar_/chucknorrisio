package co.tiagoaguiar.chucknorrisio.common.view

import io.reactivex.disposables.CompositeDisposable

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface BaseView<P> {

    fun setPresenter(presenter: P)

    fun setDisposable(disposable: CompositeDisposable)

    fun showProgressBar()

    fun hideProgressBar()

}