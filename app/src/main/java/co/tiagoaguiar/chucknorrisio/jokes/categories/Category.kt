package co.tiagoaguiar.chucknorrisio.jokes.categories

import co.tiagoaguiar.chucknorrisio.common.view.BaseView
import co.tiagoaguiar.chucknorrisio.jokes.categories.view.CategoryItem

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface Category {

    interface View : BaseView<Presenter> {

        fun showCategories(categories: List<CategoryItem>)

        fun showFailure(message: String)

    }

    interface Presenter {

        fun requestAll()

    }

}