package co.tiagoaguiar.chucknorrisio.jokes.categories.component

import co.tiagoaguiar.chucknorrisio.common.util.ViewScoped
import co.tiagoaguiar.chucknorrisio.jokes.categories.module.CategoryModule
import co.tiagoaguiar.chucknorrisio.jokes.categories.view.CategoryDrawerActivity
import dagger.Component

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@ViewScoped
@Component(dependencies = [CategoryRepositoryComponent::class], modules = [CategoryModule::class])
interface CategoryComponent {

    fun inject(activity: CategoryDrawerActivity)

}