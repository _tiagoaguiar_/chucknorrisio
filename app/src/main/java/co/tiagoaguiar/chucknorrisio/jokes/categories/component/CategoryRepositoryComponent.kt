package co.tiagoaguiar.chucknorrisio.jokes.categories.component

import co.tiagoaguiar.chucknorrisio.common.module.NetworkModule
import co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.CategoryRepository
import co.tiagoaguiar.chucknorrisio.jokes.categories.module.CategoryRepositoryModule
import dagger.Component
import javax.inject.Singleton

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Singleton
@Component(modules = [CategoryRepositoryModule::class, NetworkModule::class])
interface CategoryRepositoryComponent {

    fun getCategoryRepository(): CategoryRepository

}