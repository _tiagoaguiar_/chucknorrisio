package co.tiagoaguiar.chucknorrisio.jokes.categories.data.source

import co.tiagoaguiar.chucknorrisio.common.model.DataSource
import co.tiagoaguiar.chucknorrisio.jokes.categories.view.CategoryItem

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface CategoryDataSource {

    fun findAll(callback: ListCategoriesCallback)

    interface ListCategoriesCallback : DataSource<List<String>, List<CategoryItem>>

}