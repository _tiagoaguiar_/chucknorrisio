package co.tiagoaguiar.chucknorrisio.jokes.categories.data.source

import javax.inject.Inject

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class CategoryRepository @Inject constructor(private val mDataSource: CategoryDataSource) : CategoryDataSource {

    override fun findAll(callback: CategoryDataSource.ListCategoriesCallback) {
        mDataSource.findAll(callback)
    }

}