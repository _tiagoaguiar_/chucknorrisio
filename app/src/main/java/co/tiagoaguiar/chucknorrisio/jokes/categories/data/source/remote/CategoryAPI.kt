package co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.remote

import io.reactivex.Observable
import retrofit2.http.GET

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface CategoryAPI {

    @GET("jokes/categories")
    fun findAll() : Observable<List<String>>

}