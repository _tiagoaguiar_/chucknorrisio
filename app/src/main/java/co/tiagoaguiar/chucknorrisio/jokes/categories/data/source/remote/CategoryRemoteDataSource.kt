package co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.remote

import co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.CategoryDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Singleton
class CategoryRemoteDataSource @Inject constructor(private val mRetrofit: Retrofit) : CategoryDataSource {

    override fun findAll(callback: CategoryDataSource.ListCategoriesCallback) {
        val compositeDisposable = CompositeDisposable()

        compositeDisposable.add(
            mRetrofit.create(CategoryAPI::class.java)
                .findAll()
                .subscribeOn(Schedulers.io())
                .flatMapIterable { callback.transform(it) }
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { callback.onComplete() }
                .subscribe({ callback.onSuccess(it) }, { callback.onError(it.message ?: "Does not have error message") })
        )

        callback.onSubscribe(compositeDisposable)
    }

}