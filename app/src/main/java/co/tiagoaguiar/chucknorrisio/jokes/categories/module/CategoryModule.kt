package co.tiagoaguiar.chucknorrisio.jokes.categories.module

import co.tiagoaguiar.chucknorrisio.jokes.categories.Category
import dagger.Module
import dagger.Provides

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
class CategoryModule(private val mView: Category.View) {

    @Provides
    fun provideView() : Category.View {
        return  mView
    }

}