package co.tiagoaguiar.chucknorrisio.jokes.categories.module

import co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.CategoryDataSource
import co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.remote.CategoryRemoteDataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
abstract class CategoryRepositoryModule {

    @Singleton
    @Binds
    abstract fun provideRemoteDataSource(dataSource: CategoryRemoteDataSource): CategoryDataSource

}