package co.tiagoaguiar.chucknorrisio.jokes.categories.presenter

import co.tiagoaguiar.chucknorrisio.common.util.Colors
import co.tiagoaguiar.chucknorrisio.jokes.categories.Category
import co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.CategoryDataSource
import co.tiagoaguiar.chucknorrisio.jokes.categories.data.source.CategoryRepository
import co.tiagoaguiar.chucknorrisio.jokes.categories.view.CategoryItem
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.stream.Collectors
import javax.inject.Inject

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class CategoryPresenter : Category.Presenter, CategoryDataSource.ListCategoriesCallback {

    private val mRepository: CategoryRepository
    private val mView: Category.View

    @Inject
    constructor(repository: CategoryRepository, view: Category.View) {
        mRepository = repository
        mView = view
        mView.setPresenter(this)
    }

    override fun onSubscribe(it: CompositeDisposable) {
        mView.setDisposable(it)
        mView.showProgressBar()
    }

    override fun requestAll() {
        mRepository.findAll(this)
    }

    override fun onSuccess(response: List<CategoryItem>) {
        mView.showCategories(response)
    }

    override fun onError(message: String) {
        mView.showFailure(message)
    }

    override fun onComplete() {
        mView.hideProgressBar()
    }


    override fun transform(joke: List<String>): List<CategoryItem> {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return joke.parallelStream().map {
                CategoryItem(it.toUpperCase(), it, Colors.randomColor())
            }.collect(Collectors.toList())

        }
        val items = arrayListOf<CategoryItem>()
        joke.forEach {
            items.add(CategoryItem(it.toUpperCase(), it, Colors.randomColor()))
        }
        return items
    }


}