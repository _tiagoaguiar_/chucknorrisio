package co.tiagoaguiar.chucknorrisio.jokes.categories.view

import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.MenuItem
import co.tiagoaguiar.chucknorrisio.ChuckNorrisApplication
import co.tiagoaguiar.chucknorrisio.R
import co.tiagoaguiar.chucknorrisio.common.view.AbstractActivity
import co.tiagoaguiar.chucknorrisio.common.view.ActivityUtils.Companion.addFragmentToActivity
import co.tiagoaguiar.chucknorrisio.jokes.categories.Category
import co.tiagoaguiar.chucknorrisio.jokes.categories.component.DaggerCategoryComponent
import co.tiagoaguiar.chucknorrisio.jokes.categories.module.CategoryModule
import co.tiagoaguiar.chucknorrisio.jokes.categories.presenter.CategoryPresenter
import kotlinx.android.synthetic.main.act_drawer.*
import kotlinx.android.synthetic.main.inc_category_app_bar.*
import javax.inject.Inject

class CategoryDrawerActivity : AbstractActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var presenter: CategoryPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupNavigationDrawer()

        val fragment = addFragmentToActivity(this, CategoryFragment::class.java) as Category.View
        val categoryRepositoryComponent = (application as ChuckNorrisApplication).categoryRepositoryComponent

        DaggerCategoryComponent.builder()
            .categoryRepositoryComponent(categoryRepositoryComponent)
            .categoryModule(CategoryModule(fragment))
            .build()
            .inject(this)
    }

    private fun setupNavigationDrawer() {
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            getToolbar(),
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawer_layout.addDrawerListener(toggle)

        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        var selected = true
        if (id == R.id.menu_about) {
            var builder: AlertDialog.Builder

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = AlertDialog.Builder(this, R.style.Dialog)
            } else {
                builder = AlertDialog.Builder(this)
            }

            builder
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.about_message))
                .setPositiveButton(android.R.string.yes, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
            selected = false
        }

        drawer_layout.closeDrawer(Gravity.START)

        return selected
    }

    override fun getLayoutResID(): Int {
        return R.layout.act_drawer
    }

    override fun getToolbar(): Toolbar? {
        return category_toolbar;
    }

}
