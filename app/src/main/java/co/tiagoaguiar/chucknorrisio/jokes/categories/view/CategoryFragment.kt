package co.tiagoaguiar.chucknorrisio.jokes.categories.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import co.tiagoaguiar.chucknorrisio.R
import co.tiagoaguiar.chucknorrisio.common.view.AbstractFragment
import co.tiagoaguiar.chucknorrisio.common.view.ActivityUtils.Companion.setAnimationOnOpen
import co.tiagoaguiar.chucknorrisio.jokes.categories.Category
import co.tiagoaguiar.chucknorrisio.jokes.random.view.JokeActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.frag_category.view.*

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class CategoryFragment : AbstractFragment<Category.Presenter>(), Category.View {

    private lateinit var mAdapter: GroupAdapter<ViewHolder>

    override fun onCustomCreateView(view: View, container: ViewGroup?) {
        view.rv_main.addItemDecoration(DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL))

        mAdapter = GroupAdapter()

        view.rv_main.adapter = mAdapter
        mAdapter.setOnItemClickListener { item, _ ->
            val intent = Intent(mContext, JokeActivity::class.java)

            val categoryItem: CategoryItem = item as CategoryItem

            intent.putExtra(JokeActivity.CATEGORY_KEY, categoryItem.mCategoryUrl)
            startActivity(intent)

            setAnimationOnOpen(mContext as Activity)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.dispose()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mPresenter?.requestAll()
    }

    override fun showCategories(categories: List<CategoryItem>) {
        mAdapter.addAll(categories)
        mAdapter.notifyDataSetChanged()
    }

    override fun showFailure(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun getLayoutResID(): Int {
        return R.layout.frag_category
    }

}