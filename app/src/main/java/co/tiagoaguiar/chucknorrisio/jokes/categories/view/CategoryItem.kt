package co.tiagoaguiar.chucknorrisio.jokes.categories.view

import co.tiagoaguiar.chucknorrisio.R
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.card_category.view.*

/**
 *
 * November, 17 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class CategoryItem(
    val mCategory: String,
    val mCategoryUrl: String,
    val mBgColor: Int
) : Item<ViewHolder>() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.txt_category.text = mCategory;
        viewHolder.itemView.setBackgroundColor(mBgColor)
    }

    override fun getLayout(): Int {
        return R.layout.card_category
    }
}