package co.tiagoaguiar.chucknorrisio.jokes.random

import co.tiagoaguiar.chucknorrisio.common.view.BaseView
import co.tiagoaguiar.chucknorrisio.jokes.random.view.JokeItem

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface Joke {

    interface View : BaseView<Presenter> {

        fun showJoke(jokeItem: JokeItem)

        fun showFailure(message: String)

    }

    interface Presenter {

        fun requestRandomBy(category: String)

    }

}