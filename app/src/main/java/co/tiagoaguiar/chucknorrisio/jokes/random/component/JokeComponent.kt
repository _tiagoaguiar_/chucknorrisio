package co.tiagoaguiar.chucknorrisio.jokes.random.component

import co.tiagoaguiar.chucknorrisio.common.util.ViewScoped
import co.tiagoaguiar.chucknorrisio.jokes.random.module.JokeModule
import co.tiagoaguiar.chucknorrisio.jokes.random.view.JokeActivity
import dagger.Component

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@ViewScoped
@Component(dependencies = [JokeRepositoryComponent::class], modules = [JokeModule::class])
interface JokeComponent {

    fun inject(activity: JokeActivity)

}