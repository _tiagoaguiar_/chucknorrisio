package co.tiagoaguiar.chucknorrisio.jokes.random.component

import co.tiagoaguiar.chucknorrisio.common.module.NetworkModule
import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.JokeRepository
import co.tiagoaguiar.chucknorrisio.jokes.random.module.JokeRepositoryModule
import dagger.Component
import javax.inject.Singleton

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Singleton
@Component(modules = [JokeRepositoryModule::class, NetworkModule::class])
interface JokeRepositoryComponent {

    fun getJokeRepository() : JokeRepository

}