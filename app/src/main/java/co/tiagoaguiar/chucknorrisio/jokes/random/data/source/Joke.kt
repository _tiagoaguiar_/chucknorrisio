package co.tiagoaguiar.chucknorrisio.jokes.random.data.source

import com.google.gson.annotations.SerializedName

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
data class Joke(
    val id: String,
    @SerializedName("icon_url") val iconUrl: String,
    val url: String,
    val value: String
)