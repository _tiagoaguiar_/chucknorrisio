package co.tiagoaguiar.chucknorrisio.jokes.random.data.source

import co.tiagoaguiar.chucknorrisio.common.model.DataSource
import co.tiagoaguiar.chucknorrisio.jokes.random.view.JokeItem

interface JokeDataSource {

    fun findRandomBy(category: String, callback: JokeCallback)

    interface JokeCallback : DataSource<Joke, JokeItem>

}