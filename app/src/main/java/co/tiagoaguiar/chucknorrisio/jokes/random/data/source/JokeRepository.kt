package co.tiagoaguiar.chucknorrisio.jokes.random.data.source

import javax.inject.Inject

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class JokeRepository @Inject constructor(private val mDataSource: JokeDataSource) : JokeDataSource {

    override fun findRandomBy(category: String, callback: JokeDataSource.JokeCallback) {
        mDataSource.findRandomBy(category, callback)
    }

}