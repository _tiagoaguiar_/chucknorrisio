package co.tiagoaguiar.chucknorrisio.jokes.random.data.source.remote

import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.Joke
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
interface JokeAPI {

    @GET("jokes/random")
    fun findRandomBy(@Query("category") category: String) : Observable<Joke>

}