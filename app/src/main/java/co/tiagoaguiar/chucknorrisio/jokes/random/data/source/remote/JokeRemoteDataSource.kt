package co.tiagoaguiar.chucknorrisio.jokes.random.data.source.remote

import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.JokeDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class JokeRemoteDataSource @Inject constructor(private val mRetrofit: Retrofit) : JokeDataSource {

    override fun findRandomBy(category: String, callback: JokeDataSource.JokeCallback) {
        val compositeDisposable = CompositeDisposable()

        compositeDisposable.add(
            mRetrofit.create(JokeAPI::class.java)
                .findRandomBy(category)
                .subscribeOn(Schedulers.io())
                .map { callback.transform(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { callback.onComplete() }
                .subscribe({ callback.onSuccess(it) }, { callback.onError(it.message ?: "Does not have error") })
        )

        callback.onSubscribe(compositeDisposable)
    }

}