package co.tiagoaguiar.chucknorrisio.jokes.random.module

import co.tiagoaguiar.chucknorrisio.jokes.random.Joke
import dagger.Module
import dagger.Provides

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
class JokeModule(private val mView: Joke.View) {

    @Provides
    fun provideView() : Joke.View {
        return mView
    }

}