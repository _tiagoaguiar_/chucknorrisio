package co.tiagoaguiar.chucknorrisio.jokes.random.module

import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.JokeDataSource
import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.remote.JokeRemoteDataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
abstract class JokeRepositoryModule {

    @Singleton
    @Binds
    abstract fun provideDataSource(dataSource: JokeRemoteDataSource) : JokeDataSource

}