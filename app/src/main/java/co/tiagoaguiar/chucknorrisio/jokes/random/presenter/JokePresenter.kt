package co.tiagoaguiar.chucknorrisio.jokes.random.presenter

import co.tiagoaguiar.chucknorrisio.jokes.random.Joke
import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.JokeDataSource
import co.tiagoaguiar.chucknorrisio.jokes.random.data.source.JokeRepository
import co.tiagoaguiar.chucknorrisio.jokes.random.view.JokeItem
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class JokePresenter : Joke.Presenter, JokeDataSource.JokeCallback {

    private var mRepository: JokeRepository
    private var mView: Joke.View

    @Inject
    constructor(repository: JokeRepository, view: Joke.View) {
        mRepository = repository
        mView = view
        mView.setPresenter(this)
    }

    override fun onSubscribe(it: CompositeDisposable) {
        mView.setDisposable(it)
        mView.showProgressBar()
    }

    override fun requestRandomBy(category: String) {
        mRepository.findRandomBy(category, this)
    }

    override fun onSuccess(response: JokeItem) {
        mView.showJoke(response)
    }

    override fun onError(message: String) {
        mView.showFailure(message)
    }

    override fun onComplete() {
        mView.hideProgressBar()
    }

    override fun transform(joke: co.tiagoaguiar.chucknorrisio.jokes.random.data.source.Joke): JokeItem {
        return JokeItem(joke.iconUrl, joke.url, joke.value)
    }

}