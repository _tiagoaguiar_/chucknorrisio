package co.tiagoaguiar.chucknorrisio.jokes.random.view

import android.os.Bundle
import android.support.v7.widget.Toolbar
import co.tiagoaguiar.chucknorrisio.ChuckNorrisApplication
import co.tiagoaguiar.chucknorrisio.R
import co.tiagoaguiar.chucknorrisio.common.view.AbstractActivity
import co.tiagoaguiar.chucknorrisio.common.view.ActivityUtils
import co.tiagoaguiar.chucknorrisio.common.view.ActivityUtils.Companion.addFragmentToActivity
import co.tiagoaguiar.chucknorrisio.jokes.random.component.DaggerJokeComponent
import co.tiagoaguiar.chucknorrisio.jokes.random.module.JokeModule
import co.tiagoaguiar.chucknorrisio.jokes.random.presenter.JokePresenter
import kotlinx.android.synthetic.main.act_joke.*
import javax.inject.Inject

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class JokeActivity : AbstractActivity() {

    companion object {
        const val CATEGORY_KEY = "category_key"
    }

    @Inject
    lateinit var presenter: JokePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val fragment: JokeFragment = addFragmentToActivity(this, JokeFragment::class.java) as JokeFragment
        fragment.arguments = intent.extras

        val jokeRepositoryComponent = (application as ChuckNorrisApplication).jokeRepositoryComponent

        DaggerJokeComponent.builder()
            .jokeRepositoryComponent(jokeRepositoryComponent)
            .jokeModule(JokeModule(fragment))
            .build()
            .inject(this)

        btn_refresh.setOnClickListener(fragment)
    }

    override fun getToolbarTitle(): String {
        return intent?.extras?.getString(CATEGORY_KEY, super.getToolbarTitle()) ?: super.getToolbarTitle()
    }

    override fun getToolbar(): Toolbar? {
        return toolbar
    }

    override fun getLayoutResID(): Int {
        return R.layout.act_joke
    }

}