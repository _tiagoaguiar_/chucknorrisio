package co.tiagoaguiar.chucknorrisio.jokes.random.view

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import co.tiagoaguiar.chucknorrisio.R
import co.tiagoaguiar.chucknorrisio.common.view.AbstractFragment
import co.tiagoaguiar.chucknorrisio.jokes.random.Joke
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.frag_joke.*

/**
 *
 * November, 18 2018
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
class JokeFragment : AbstractFragment<Joke.Presenter>(), Joke.View, View.OnClickListener {

    private var mCategory: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mCategory = arguments?.getString(JokeActivity.CATEGORY_KEY)
    }

    override fun onCustomCreateView(view: View, container: ViewGroup?) {
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.dispose()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mCategory?.let {
            mPresenter?.requestRandomBy(it)
        }
    }

    override fun onClick(v: View?) {
        mCategory?.let {
            mPresenter?.requestRandomBy(it)
        }
    }

    override fun showJoke(jokeItem: JokeItem) {
        txt_joke.text = jokeItem.text
        Picasso.get().load(jokeItem.iconUrl).into(img_icon)
    }

    override fun showFailure(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun getLayoutResID(): Int {
        return R.layout.frag_joke
    }

}