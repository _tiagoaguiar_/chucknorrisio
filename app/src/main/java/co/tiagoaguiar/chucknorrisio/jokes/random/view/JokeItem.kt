package co.tiagoaguiar.chucknorrisio.jokes.random.view

data class JokeItem(val iconUrl: String, val url: String, val text: String)